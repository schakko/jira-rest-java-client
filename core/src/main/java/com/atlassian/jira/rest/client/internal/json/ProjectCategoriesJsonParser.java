package com.atlassian.jira.rest.client.internal.json;

import java.util.ArrayList;

import com.atlassian.jira.rest.client.api.domain.ProjectCategory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

/**
 * @author swe
 */
public class ProjectCategoriesJsonParser implements JsonArrayParser<Iterable<ProjectCategory>> {

	ProjectCategoryJsonParser projectCategoryJsonParser = new ProjectCategoryJsonParser();

	@Override
	public Iterable<ProjectCategory> parse(JSONArray json) throws JSONException {
		ArrayList<ProjectCategory> res = new ArrayList<ProjectCategory>(json.length());

		for (int i = 0; i < json.length(); i++) {
			res.add(projectCategoryJsonParser.parse(json.getJSONObject(i)));
		}

		return res;
	}

}
