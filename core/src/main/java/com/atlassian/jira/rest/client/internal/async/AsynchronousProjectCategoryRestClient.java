package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.ProjectCategoryRestClient;
import com.atlassian.jira.rest.client.api.domain.ProjectCategory;
import com.atlassian.jira.rest.client.api.domain.input.ProjectCategoryInput;
import com.atlassian.jira.rest.client.internal.json.ProjectCategoriesJsonParser;
import com.atlassian.jira.rest.client.internal.json.ProjectCategoryJsonParser;
import com.atlassian.jira.rest.client.internal.json.gen.ProjectCategoryInputJsonGenerator;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousProjectCategoryRestClient extends AbstractAsynchronousRestClient
		implements ProjectCategoryRestClient {

	private static final String PROJECT_CATEGORY_URI_PREFIX = "projectCategory";

	private final ProjectCategoriesJsonParser projectCategoriesJsonParser = new ProjectCategoriesJsonParser();

	private final ProjectCategoryJsonParser projectCategoryJsonParser = new ProjectCategoryJsonParser();

	private final URI baseUri;

	public AsynchronousProjectCategoryRestClient(final URI baseUri, final HttpClient client) {
		super(client);
		this.baseUri = baseUri;
	}

	@Override
	public Promise<ProjectCategory> getProjectCategory(Long id) {
		String key = String.valueOf(id);
		final URI uri = UriBuilder.fromUri(baseUri).path(PROJECT_CATEGORY_URI_PREFIX).path(key).build();
		return getAndParse(uri, projectCategoryJsonParser);
	}

	@Override
	public Promise<ProjectCategory> getProjectCategory(URI projectCategoryUri) {
		return getAndParse(projectCategoryUri, projectCategoryJsonParser);
	}

	@Override
	public Promise<Iterable<ProjectCategory>> getAllProjectCategories() {
		final URI uri = UriBuilder.fromUri(baseUri).path(PROJECT_CATEGORY_URI_PREFIX).build();
		return getAndParse(uri, projectCategoriesJsonParser);
	}

	@Override
	public Promise<ProjectCategory> createProjectCategory(final ProjectCategoryInput issue) {
		final UriBuilder uriBuilder = UriBuilder.fromUri(baseUri).path(PROJECT_CATEGORY_URI_PREFIX);
		
		return postAndParse(uriBuilder.build(), issue, new ProjectCategoryInputJsonGenerator(),
				projectCategoryJsonParser);
	}
	
	@Override
	public Promise<ProjectCategory> updateProjectCategory(Long id, ProjectCategoryInput projectCategory) {
		final UriBuilder uriBuilder = UriBuilder.fromUri(baseUri).path(PROJECT_CATEGORY_URI_PREFIX).path(String.valueOf(id));
		
		return putAndParse(uriBuilder.build(), projectCategory, new ProjectCategoryInputJsonGenerator(), projectCategoryJsonParser);
	}

	@Override
	public Promise<Void> removeProjectCategory(final URI projectCategoryUri) {
		final UriBuilder uriBuilder = UriBuilder.fromUri(projectCategoryUri);
		return delete(uriBuilder.build());
	}

}
