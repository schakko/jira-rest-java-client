/*
 * Copyright (C) 2010 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.internal.json;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.joda.time.DateTime;

import com.atlassian.jira.rest.client.api.domain.WorklogQuery;

/**
 * 
 * @author christopher[dot]klein[at]neos-it[dot]de
 * @since 3.0.2
 */
public class WorklogQueryJsonParser implements JsonObjectParser<WorklogQuery> {

	@Override
	public WorklogQuery parse(JSONObject json) throws JSONException {
		final long id = json.getLong("id");
		String plainStartDate = json.getString("startDate");

		final DateTime startDate = JsonParseUtil.parseDateTimeWithTimezone(plainStartDate);
		final String issueKey = json.getString("issueKey");
		final String userId = json.getString("userId");
		final int duration = json.getInt("duration");
		final String comment = JsonParseUtil.getNullableString(json, "comment");
		DateTime updateDate = null;

		String plainUpdateDate = json.optString("updated");

		if (!StringUtils.isEmpty(plainUpdateDate)) {
			updateDate = JsonParseUtil.parseDateTimeWithTimezone(plainUpdateDate);
		}

		return new WorklogQuery(id, startDate, issueKey, userId, duration, updateDate, comment);
	}
}
