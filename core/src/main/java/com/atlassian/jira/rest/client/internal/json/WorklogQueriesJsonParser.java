package com.atlassian.jira.rest.client.internal.json;

import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import com.atlassian.jira.rest.client.api.domain.WorklogQuery;

/**
 * @author christopher[dot]klein[at]neos-it[dot]de
 * @since 3.0.2
 */
public class WorklogQueriesJsonParser implements JsonArrayParser<Iterable<WorklogQuery>> {

	WorklogQueryJsonParser worklogQueryJsonParser = new WorklogQueryJsonParser();

	@Override
	public Iterable<WorklogQuery> parse(JSONArray json) throws JSONException {
		if (json.length() != 1) {
			throw new JSONException("WorklogQuery response must consist of an array of array: [[{...}]]");
		}

		JSONArray innerArray = json.getJSONArray(0);
		ArrayList<WorklogQuery> res = new ArrayList<WorklogQuery>(innerArray.length());

		for (int i = 0; i < innerArray.length(); i++) {
			res.add(worklogQueryJsonParser.parse(innerArray.getJSONObject(i)));
		}

		return res;
	}

}
