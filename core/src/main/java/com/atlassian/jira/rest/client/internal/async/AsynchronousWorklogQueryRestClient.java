/*
 * Copyright (C) 2012 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang.StringUtils;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.WorklogQueryRestClient;
import com.atlassian.jira.rest.client.api.domain.WorklogQuery;
import com.atlassian.jira.rest.client.internal.json.WorklogQueriesJsonParser;
import com.atlassian.util.concurrent.Promise;

/**
 * Asynchronous implementation of {@link WorklogQueryRestClient}.
 *
 * @since 3.0.2
 */
public class AsynchronousWorklogQueryRestClient extends AbstractAsynchronousRestClient
		implements WorklogQueryRestClient {

	private static final String PATH_REST_PLUGIN = "/rest/jira-worklog-query";
	public final static String LATEST_VERSION = "2";
	private final URI baseURI;
	private final URI worklogQueryRootUri;
	private String endpointVersion = LATEST_VERSION;
	private WorklogQueriesJsonParser parser = new WorklogQueriesJsonParser();

	/**
	 * Initializes this class with the endpoint version {@value #LATEST_VERSION}
	 * .
	 * 
	 * @param baseUri
	 *            not null
	 * @param client
	 *            not null
	 */
	public AsynchronousWorklogQueryRestClient(URI serverUri, final HttpClient client) {
		this(serverUri, client, LATEST_VERSION);
	}

	/**
	 * Initializes the class with a custom endpoint version
	 * 
	 * @param serverUri
	 *            not null
	 * @param client
	 *            not null
	 * @param worklogQueryVersion
	 *            if empty, {@value #LATEST_VERSION} is used
	 */
	public AsynchronousWorklogQueryRestClient(URI serverUri, final HttpClient client, String worklogQueryVersion) {
		super(client);

		this.baseURI = serverUri;

		if (!StringUtils.isEmpty(worklogQueryVersion)) {
			endpointVersion = worklogQueryVersion;
		}

		worklogQueryRootUri = UriBuilder.fromUri(serverUri).path(PATH_REST_PLUGIN).path(endpointVersion).build();
	}

	public AsynchronousWorklogQueryRestClient inVersion(String version) {
		if (endpointVersion.equals(version)) {
			return this;
		}

		return new AsynchronousWorklogQueryRestClient(this.baseURI, client(), version);
	}

	@Override
	public Promise<Iterable<WorklogQuery>> findWorklogs(String startDate, @Nullable String endDate, String user,
			String group, @Nullable String project, @Nullable String[] fields) {
		if (StringUtils.isEmpty(startDate)) {
			throw new IllegalArgumentException("startDate must not be empty");
		}

		if (StringUtils.isEmpty(user) && StringUtils.isEmpty(group)) {
			throw new IllegalArgumentException("one of parameters 'user' or 'group' must not be empty");
		}

		UriBuilder uriBuilder = UriBuilder.fromUri(worklogQueryRootUri).path("find").path("worklogs");
		uriBuilder.queryParam("startDate", startDate);

		if (endDate != null) {
			uriBuilder.queryParam("endDate", endDate);
		}

		if (user != null) {
			uriBuilder.queryParam("user", user);
		}

		if (group != null) {
			uriBuilder.queryParam("group", group);
		}

		if (project != null) {
			uriBuilder.queryParam("project", project);
		}

		if (fields != null) {
			uriBuilder.queryParam("fields", StringUtils.join(fields, ","));
		}

		return getAndParse(uriBuilder.build(), parser);
	}
}
