package com.atlassian.jira.rest.client.api.domain.input;

import java.net.URI;

import javax.annotation.Nullable;

/**
 * 
 * @author sebastian[dot]weinert[at]neos-it[dot]de
 */
public class ProjectCategoryInput {
	@Nullable
	private final URI self;

	private final String name;
	private final String description;

	public ProjectCategoryInput(@Nullable URI self, String name, String description) {
		this.name = name;
		this.description = description;
		this.self = self;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public URI getSelf() {
		return self;
	}

}
